<?php

include "conectasql.php";
$curso = $_POST["curso_selecionado"];


$listas_turma = $conexao->prepare("SELECT t.CODIGO, DATE_FORMAT(t.DATA_INICIAL, '%d/%m/%Y') as DATA_INICIAL FROM turma t WHERE t.ID_EVENTO = (?) AND DATA_INICIAL >= CURDATE() order by t.DATA_INICIAL");
$listas_turma -> bind_param("i",$curso);
$listas_turma -> execute();
$turma_resultado = $listas_turma ->get_result();
$turmas = array();

$i = 0;
while ($n = $turma_resultado -> fetch_assoc()) {
    $turmas[$i]["id"] = utf8_encode($n['CODIGO']);
    $turmas[$i]["label"] = utf8_encode($n['CODIGO']);
    $turmas[$i]["value"] = utf8_encode($n['CODIGO']);
    $turmas[$i]["data"] = $n['DATA_INICIAL'];
    $i = $i + 1;
}

echo json_encode($turmas);
?>