<script src="jquery-ui.js"></script>

<script>
$(document).ready(function () {
  atualizaVendas(0,"recuperaLivros.php");
  $( "#comprador" ).autocomplete({
    source: "listanomes.php",
    minLength: 6,
  });

  var lista_livro = $( "#answ_livro" ).autocomplete({
      source: "listalivro.php",
      minLength: 1,
      select: function (event, ui) {
          $("#id_livro").val(ui.item.id);;
      },
      change: function(event, ui) {
        if (ui.item == null) {
          alert("Selecione um livro válido");
          $("#answ_livro").val("");
          $("#answ_livro").focus();
        }
      }    
  });

  $('form').on('submit', function (e) {
    var tel = document.getElementById("telefone").value;
    var cpf = document.getElementById("cpf").value;
    var email = document.getElementById("email").value;
    if(codigo == -1 && tel.trim() == '' && cpf.trim() == '' && email.trim() == ''){
        e.preventDefault(); //prevent to reload the page
        alert('Como o aluno não está no IIPCNET, informe pelo menos 1 desses campos: Telefone, Email ou CPF');
    }else{
    e.preventDefault(); //prevent to reload the page
    $.ajax({
        type: 'POST', 
        url: 'enviaVenda.php',
        data: $('form').serialize()+"&cod_iipcnet="+$("#atendidoID").val()+"&tipo_venda="+tipo_venda+"&fl_sem_iipcnet="+$("#fl_sem_iipcnet").val(),
        success: function () {
            alert('Enviado com sucesso!');
            //location.reload();
            insereVendas($("#answ_livro").val(),$("#atendido").val(),$("#valor").val());
            $("#frm_livro")[0].reset();
        }
    });
    }
  });
});
</script>


<form id="frm_livro">
  <div class="row">
    <div class="col-md-12">
      <h4 class="">Venda de Livro</h4>
    </div>
  </div>

  <?php
    include "comprador.php";
  ?>

  <div class ="row mb-3">
    <div class="col-md-9">
      <label for="livro">Nome do livro</label>
      <input type="text" class="form-control" id="answ_livro" name="answ_livro" placeholder="" value="" required="">
      <input type="hidden" id="id_livro" name="id_livro"/>
    </div>
    <div class="col-md-3">
      <label for="quantidade">Quantidade</label>
      <input type="number" class="form-control" id="quantidade" min="1" name="quantidade" placeholder="" value="1" required="">
    </div>
  </div>

  <?php
    include "origem_aluno.php";
  ?>

  <div class="row">
    <div class="col-md-12">
      <h4 class="">Pagamento</h4>
    </div>
  </div>
  
  <?php
    include "pagamento.php";
  ?>

  <div class="row">
    <button class="btn btn-primary btn-lg btn-block" type="submit">Enviar</button>
  </div>

    <?php
    include "vendas_lancadas.php";
  ?>
  <script src="popper.min.js" crossorigin="anonymous"></script>
  <script src="vendas_lancadas.js"></script>  
  <script src="bootstrap-4.0.0-beta.3/dist/js/bootstrap.min.js" crossorigin="anonymous"></script>
</form>