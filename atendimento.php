<script src="jquery-3.3.1.min.js" crossorigin="anonymous"></script>

<?php

include "conectasql.php";

$cursos_interesse = "SELECT DISTINCT e.* FROM `evento_ativo` e where e.ativo = 1";
$res_curso_ativo = $conexao ->query($cursos_interesse);

$origem = "SELECT DISTINCT o.* FROM `tipo_origem` o";
$res_origem = $conexao ->query($origem);

?>

<script src="jquery-ui.js"></script>
<script src="jQuery-Plugin-For-Multi-Select-Checboxes-multiselect/js/jquery.multiselect.js"></script>
<link rel="stylesheet" href="jQuery-Plugin-For-Multi-Select-Checboxes-multiselect/css/jquery.multiselect.css">


<script>
    var counter_curso = 1;             
    function addInput_curso(divName){
      counter_curso++;
      var newdiv = document.createElement('div');
      newdiv.id = "dynamicInput_curso_child"+counter_curso;
      newdiv.innerHTML = " <br><input id='select_interesse_curso"+ counter_curso +"' style='background-color: white;' type='text' class='form-control input-lg interesse_extra'> <input type='hidden' id='select_interesse_cursoID"+ counter_curso +"' type='text' class='interesse_extra' name='select_interesse_curso[]'>";
      document.getElementById(divName).appendChild(newdiv);

      $( "#select_interesse_curso"+counter_curso+"").autocomplete({
            source: "listacurso.php",
            minLength: 3,
            select: function (event, ui) {
                $("#select_interesse_cursoID"+ counter_curso +"").val(ui.item.id);
            }
      });   
    }

    var counter_livro = 1;             
    function addInput_livro(divName){
      counter_livro++;
      var newdiv = document.createElement('div');
      newdiv.id = "dynamicInput_livro_child"+counter_livro;
      newdiv.innerHTML = " <br><input id='select_interesse_livro"+ counter_livro +"' style='background-color: white;' type='text' class='form-control input-lg interesse_extra'> <input type='hidden' id='select_interesse_livroID"+ counter_livro +"' type='text' class='interesse_extra' name='select_interesse_livro[]'>";
      document.getElementById(divName).appendChild(newdiv);

        $( "#select_interesse_livro"+counter_livro+"").autocomplete({
            source: "listalivro.php",
            minLength: 3,
            select: function (event, ui) {
                    $("#select_interesse_livroID"+ counter_livro +"").val(ui.item.id);
            }            
        });
           
    }


    function removeInput_curso(){
      if (counter_curso > 1) {
              $( "#dynamicInput_curso_child"+counter_curso ).remove();
              counter_curso--;
      }
    }

    function removeInput_livro(){
      if (counter_livro > 1) {
              $( "#dynamicInput_livro_child"+counter_livro ).remove();
              counter_livro--;
      }
    }
    
    var codigo = 0;
    $(document).ready(function () {
        $( "#atendido" ).autocomplete({
            source: "listanomes.php",
            minLength: 3,
            select: function (event, ui) {
                $("#atendidoID").val(ui.item.id); 
                $("#fl_sem_iipcnet").val(ui.item.fl_sem_iipcnet);
            },
            change: function(event, ui) {
                if (ui.item == null) {                   
                    $("#fl_sem_iipcnet").val(1);
                    $("#div_aluno_sem_cadastro").show();
                }else{
                    $("#div_aluno_sem_cadastro").hide();
                    // está cadaastrado
                }
            }  
        });

        $( "#select_interesse_curso1" ).autocomplete({
            source: "listacurso.php",
            minLength: 1,
            select: function (event, ui) {
                $("#select_interesse_cursoID1").val(ui.item.id); 
            },
      });

        var lista_livro = $( "#select_interesse_livro1" ).autocomplete({
            source: "listalivro.php",
            minLength: 1,
            select: function (event, ui) {
                $("#select_interesse_livroID1").val(ui.item.id);
            }    
        });

        $('#interessado_curso').click(function () {
            if ($(this).is(':checked')) {
                    $("#div_cursos_interesse").show();
            }
            else{
                    $("#div_cursos_interesse").hide();
            }
        });

        $('#interessado_livro').click(function () {
            if ($(this).is(':checked')) {
                    $("#div_livros_interesse").show();
            }
            else{
                    $("#div_livros_interesse").hide();
            }
        });

        $('form').on('submit', function (e) {
            var tel = document.getElementById("telefone").value;
            var cpf = document.getElementById("cpf").value;
            var email = document.getElementById("email").value;
            if(codigo == -1 && tel.trim() == '' && cpf.trim() == '' && email.trim() == ''){
                e.preventDefault(); //prevent to reload the page
                alert('Como o aluno não está no IIPCNET, informe pelo menos 1 desses campos: Telefone, Email ou CPF');
            }else{
            e.preventDefault(); //prevent to reload the page
            $.ajax({
                type: 'POST', //hide url
                url: 'enviaAtendimento.php', //your form validation url
                data: $('form').serialize()+"&cod_iipcnet="+$("#atendidoID").val()+"&tipo_atendimento="+tipo_atendimento+"&fl_sem_iipcnet="+$("#fl_sem_iipcnet").val(),
                success: function () {
                    alert('Enviado com sucesso!'); //display an alert whether the form is submitted okay
                    //location.reload();
                }
            });
            }
        });
      
    });
</script>

<form>
<div class="row">
    <div class="col-md-12 mb-3">
        <label for="atendido">Nome completo do atendido</label>
        <input type="text" class="form-control" id="atendido" name="atendido" placeholder="" value="" required="">
        <input type="hidden" class="form-control" id="atendidoID" placeholder="" value="0" required="">
        <input type="hidden" class="form-control" id="fl_sem_iipcnet" required="">
    </div>
</div>

<div id="div_aluno_sem_cadastro" class="card mb-4" style="display: none;">
    <div class="row mb-2">
        <div class="col-md-12" style="text-align: center;">
            <label style="color: red; margin-top: 15px; font-size: 15px;">Aluno sem cadastro no IIPCNet</label>
        </div>
    </div>
    <div class="row mb-2">
        <div class="col-md-5 offset-1">
            <label for="telefone">Telefone do atendido</label>
            <input type="text" maxlength="20" style="background-color: white;" class="form-control input-lg" id="telefone" name="telefone">
        </div>
        <div class="col-md-5">
            <label for="cpf">CPF do atendido</label>
            <input type="text"  maxlength="11" style="background-color: white;" class="form-control input-lg" id="cpf" name="cpf">
            <small class="text-muted">Apenas números</small>
        </div>
    </div>  
    <div class="row mb-5">
        <div class="col-md-10 offset-1">
            <label for="email">E-mail do atendido</label>
            <input type="email" style="background-color: white;" class="form-control input-lg" id="email" name="email">
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 mb-3">
        <label for="atendido">Como soube do IIPC ou do evento?</label>
        <select class="custom-select d-block w-100" name="origem" required="">
        <option disabled selected value>Escolha...</option>
        <?php 
          while ($linha_origem = $res_origem -> fetch_assoc()){
            ?>
              <option value="<?=$linha_origem['id']?>"><?=UTF8_ENCODE($linha_origem['descricao'])?>
              </option>
            <?php 
          }  
      ?>
      </select>
    </div>
</div>

<div class="row">
    <div class="col-md-12 mb-4">
        <input style="margin-left: 0px" class="form-check-input" type="checkbox" value="1" id="interessado_curso" name="interessado_curso">
        <label style="margin-left: 25px" class="form-check-label" for="interessado_curso">
            Demonstrou interesse em algum curso?
        </label>
        <div style="display: none;" class="row" id="div_cursos_interesse">
            <div class="col-md-12">
                <label for="select_interesse_curso1">Quais?</label>
            </div>
            <div class="col-md-12">
                <div>
                    <input type="text" style="background-color: white;" class="form-control input-lg" id="select_interesse_curso1">
                    <input type='hidden' id='select_interesse_cursoID1' type='text' name='select_interesse_curso[]'>
                </div>                                                            
                <div id="dynamicInput_curso"></div>
                <input style="color: blue; background-color: #f8f9fa; margin-top: 5px; font-size:13px; border: none; outline:none;" type="button" value="Adicionar outro campo para interesse..." onClick="addInput_curso('dynamicInput_curso');">
                <input style="color: blue; background-color: #f8f9fa; font-size:13px;  border: none; outline:none;" type="button" value="Remover último campo de interesse" onClick="removeInput_curso()">
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 mb-4">
        <input style="margin-left: 0px" class="form-check-input" type="checkbox" value="1" id="interessado_livro" name="interessado_livro">
            <label style="margin-left: 25px" class="form-check-label" for="defaultCheck1">
                Demonstrou interesse em algum livro?
            </label>
        <div style="display: none;" class="row" id="div_livros_interesse">
            <div class="col-md-12">
                <label for="select_interesse_livro1">Quais?</label>
            </div>
            <div class="col-md-12">
                <div>
                    <input type="text" style="background-color: white;" class="form-control input-lg" id="select_interesse_livro1">
                    <input type='hidden' id='select_interesse_livroID1' type='text' name='select_interesse_livro[]'>
                </div>                                                            
                <div id="dynamicInput_livro">
                </div>
                <input style="color: blue; background-color: #f8f9fa; margin-top: 5px; font-size:13px;  border: none; outline:none;" type="button" value="Adicionar outro campo para interesse..." onClick="addInput_livro('dynamicInput_livro');">
                <input style="color: blue; background-color: #f8f9fa; font-size:13px;  border: none; outline:none;" type="button" value="Remover último campo de interesse" onClick="removeInput_livro()">
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 mb-4">
    <label for="demanda">Demanda do atendido</label>
    <textarea type="text" class="form-control input-lg" id="demanda" name="demanda" placeholder="" value="" required="" rows="5" style="overflow:hidden"></textarea>
    </div>
</div>

<div class="row">
    <button class="btn btn-primary btn-lg btn-block" type="submit">Enviar</button>
</div>
<script src="popper.min.js" crossorigin="anonymous"></script>
<script src="bootstrap-4.0.0-beta.3/dist/js/bootstrap.min.js" crossorigin="anonymous"></script>
</form>