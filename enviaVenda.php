<?php
include "conectasql.php";
session_start();

$aluno_codigo = $_POST['cod_iipcnet'];
$tipo_venda = $_POST['tipo_venda'];
$fl_sem_iipcnet = $_POST['fl_sem_iipcnet'];

//se o aluno não possui código iipcnet
if($aluno_codigo == 0){
	include "enviaAtendidoSemIipcnet.php";	
}

$valor = str_replace("R$ ", '', $_POST['valor']);
$valor = str_replace(",", '.', $valor);
$valor = (float)$valor;
//var_dump($_POST['curso_selecionado']);
$sql = $conexao->prepare("INSERT INTO pagamento (tipo_pagamento, bandeira_cartao, num_autorizacao, valor,
                parcelas, data)
                            VALUES (?, ?, ?, ?, ?, now())");  

$sql ->bind_param("isidi", $_POST['tipo_pagamento'],$_POST['bandeira'], $_POST['autorizacao'], $valor, $_POST['parcelas']); 

$res = $sql->execute();
$sql->close();

$id_pagamento = mysqli_insert_id($conexao);
	mysqli_commit($conexao);
	if(is_numeric($id_pagamento)){
		echo 1; // Deu certo
    }	

    $aluno_codigo = (int) $aluno_codigo;
    

if($_POST['tipo_venda'] == 1){    
    $id_livro = (int)$_POST['id_livro']; 
    $quantidade = (int)$_POST['quantidade'];
    $sql = $conexao->prepare("INSERT INTO venda (id_aluno, id_livro, quantidade,
                    data, id_pagamento, fl_sem_iipcnet)
                                VALUES (?, ?, ?, now(), ?, ?)");  

    $sql ->bind_param("iiiii", $aluno_codigo, $id_livro, $quantidade, $id_pagamento, $fl_sem_iipcnet); 
    $res = $sql->execute();
    $sql->close();

}else if($_POST['tipo_venda'] == 2){
    $quantidade = 1;
    $sql = $conexao->prepare("INSERT INTO venda (id_aluno, id_evento, quantidade,
    data, id_pagamento, fl_sem_iipcnet)
                VALUES (?, ?, ?, now(), ?, ?)"); 

    $sql ->bind_param("iiiii", $aluno_codigo, $_POST['curso_selecionado'], $quantidade, $id_pagamento, $fl_sem_iipcnet); 
    $res = $sql->execute();
    $sql->close();
}
