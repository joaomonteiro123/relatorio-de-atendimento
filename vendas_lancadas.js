//$("#botao-sync").click(sincronizaPlantonista);

function insereVendas(nome_venda, nome_comprador,valor) {
    var corpoTabela = $(".vendas_lancadas").find("tbody");
    var linha = novaLinha(nome_venda, nome_comprador,"R$"+valor);
    corpoTabela.prepend(linha);
    $(".vendas_lancados").slideDown(500);
    scrollVendasLancados();
}

function scrollVendasLancados() {
    var posicaoVendas = $(".vendas_lancadas").offset().top;
    $("body").animate(
    {
        scrollTop: 500 + "px"
    }, 50);
}

function novaLinha(nome_venda, nome_comprador, valor) {
    var linha = $("<tr>");
    var colunaCodigo = $("<td>").text(nome_venda);
    var colunaUsuario = $("<td>").text(nome_comprador);
    var colunaValor = $("<td>").text(valor);

    linha.append(colunaCodigo);
    linha.append(colunaUsuario);
    linha.append(colunaValor);

    return linha;
}


function sincronizaVendasLancados(){

    var vendas = [];
    var linhas = $("tbody>tr");

    linhas.each(function(){
        var usuario = $(this).find("td:nth-child(1)").text();

        var score = {
            usuario: usuario           
        };

        vendas.push(score);

        var dados = {
            vendas: vendas
        };

        $.post("http://localhost:3000/vendas_lancados", dados , function() {
            console.log("Vendas sincronizado com sucesso");
            $(".tooltip").tooltipster("open"); 
        }).fail(function(){
            $(".tooltip").tooltipster("open").tooltipster("content", "Falha ao sincronizar"); 
        }).always(function(){ //novo
            setTimeout(function() {
            $(".tooltip").tooltipster("close"); 
        }, 1200);
});

    });
}


function atualizaVendas(scroll,url){
    $.get(url,function(data){
        var obj =  JSON.parse(data);
        for(i=1; i<obj.length; i++){
            var linha;
            if(url == "recuperaCursos.php"){
                linha = novaLinha(obj[i].nome_curso,obj[i].nome_comprador,"R$"+obj[i].valor);
            }else{
                linha = novaLinha(obj[i].nome_livro,obj[i].nome_comprador,"R$"+obj[i].valor);

            }
            var corpoTabela = $(".vendas_lancadas").find("tbody");
            corpoTabela.append(linha);
            $(".vendas_lancadas").slideDown(500);        
        };
        if(scroll == 1){
            scrollVendasLancados();
        }
    });
}