
<?php

//DATA DO RELATÓRIO
date_default_timezone_set('America/Sao_Paulo');
$data = date('d/m/Y', time());

include "../conectasql.php";
// ATENDIMENTOS
$atendimentos = "SELECT TIME(a.data_atendimento) as horas, ta.descricao AS tipo_atendimento, a.descricao, 
ifnull(al.NOME, asi.nome) AS nome, ifnull(al.telefone1, asi.telefone) as telefone, 
ifnull(al.CODIGO, '-') AS codiipcnet, l.nome as livro, e.NOME as evento
FROM atendimento a inner join tipo_atendimento ta on a.tipo_atendimento = ta.id
left join aluno al on al.CODIGO = a.id_aluno left join atendido_sem_iipcnet asi on asi.id = a.id_aluno
left join livro_interesse li on li.id_relatorio_atendimento = a.id_atendimento
LEFT join livro l on l.idLivro = li.id_livro left join evento_interesse ei on ei.id_relatorio_atendimento = a.id_atendimento
left join evento e on e.id = ei.evento_id WHERE DATE(a.data_atendimento) = DATE(NOW())";
$res_atendimentos = $conexao ->query($atendimentos);

$num_atendimentos = mysqli_num_rows($res_atendimentos);


//VENDAS DE LIVROS
$vendas = "SELECT TIME(v.data) AS horario,
    ifnull(al.NOME, asi.nome) AS nome,  
	ifnull(al.telefone1, asi.telefone) as telefone,  
	ifnull(al.CODIGO, '-') AS codiipcnet, 
	ifnull(l.nome,e.NOME) as produto,
    ifnull(v.quantidade, 1) as quantidade,
    p.valor,
    tp.descricao
	from venda v inner join pagamento p on v.id_pagamento = p.id_pagamento 
    inner join tipo_pagamento tp on tp.id = p.tipo_pagamento
    left join aluno al on al.CODIGO = v.id_aluno left join atendido_sem_iipcnet asi on asi.id = v.id_aluno 
	LEFT join livro l on l.idLivro = v.id_livro
	left join evento e on e.id = v.id_evento
    WHERE DATE(v.data) = DATE(NOW())";
$res_vendas = $conexao ->query($vendas);

$total_vendaslivros = "SELECT SUM(v.quantidade) as total FROM venda v
WHERE DATE(v.data) = CURRENT_DATE AND v.id_livro IS NOT NULL";
$res_total_vendaslivros = $conexao ->query($total_vendaslivros);
$total_livros = $res_total_vendaslivros -> fetch_assoc();
if (is_null($total_livros['total']) === true) {
    $total_livros['total'] = 0;
}


//VENDAS DE CURSOS
$vendascursos = "SELECT TIME(v.data) as horario,
    ifnull(al.NOME, asi.nome) AS nome,  
	ifnull(al.telefone1, asi.telefone) as telefone,  
	ifnull(al.CODIGO, '-') AS codiipcnet, 
	ifnull(l.nome,e.NOME) as produto,
    ifnull(v.quantidade, 1) as quantidade,
    p.valor,
    tp.descricao
	from venda v inner join pagamento p on v.id_pagamento = p.id_pagamento 
    inner join tipo_pagamento tp on tp.id = p.tipo_pagamento
    left join aluno al on al.CODIGO = v.id_aluno left join atendido_sem_iipcnet asi on asi.id = v.id_aluno 
	LEFT join livro l on l.idLivro = v.id_livro
	left join evento e on e.id = v.id_evento
    WHERE DATE(v.data) = DATE(NOW()) AND v.id_evento IS NOT NULL";
$res_vendascursos = $conexao ->query($vendascursos);

$num_cursos = mysqli_num_rows($res_vendascursos);

//ENCOMENDAS
$encomendas = "SELECT COUNT(*) AS total FROM `encomenda` WHERE DATE(`data`) = DATE(NOW())";
$res_encomendas = $conexao -> query($encomendas);
$total_encomendas = $res_encomendas -> fetch_assoc();

//TENEPES
$tenepes = "SELECT COUNT(*) as total FROM `tenepes` WHERE DATE(`data`) = DATE(NOW())";
$res_tenepes = $conexao -> query($tenepes);
$total_tenepes = $res_tenepes -> fetch_assoc();

?>



<div style="display: table">
    <div style="text-align: center; display: cell;">
        <h2>Relatório de Atendimento - <?=$data?></h2>
    </div>

    <div style="display: cell; text-align: center;">
        <h3>Resumo<h3>
        <div style="margin-bottom: 2%; margin-right: 1%; margin-left: 1%; border:1px solid rgba(0,0,0,.125); border-radius: .25rem; text-align: center;">
            <table style="width:100%; border-collapse: collapse; font-family: arial, sans-serif; text-align: left">
                <tbody>
                    <tr>
                        <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;">Atendimentos</td>
                        <td style="border: 1px solid #dddddd; text-align: right; padding: 8px;"><?=$num_atendimentos?></td>
                    </tr>
                    <tr>
                        <td style="background-color: #f2f2f2; border: 1px solid #dddddd; text-align: left; padding: 8px;">Vendas</td>
                        <td style="background-color: #f2f2f2; border: 1px solid #dddddd; text-align: right; padding: 8px;"><?=$total_livros['total']+$num_cursos?></td>
                    </tr>
                    <tr>
                        <td style="\border: 1px solid #dddddd; text-align: left; padding: 8px;">Pedido de Tenepes</td>
                        <td style="border: 1px solid #dddddd; text-align: right; padding: 8px;"><?=$total_tenepes['total']?></td>
                    </tr>
                    <tr>
                        <td style="background-color: #f2f2f2; border: 1px solid #dddddd; text-align: left; padding: 8px;">Correspondências Recebidas</td>
                        <td style="background-color: #f2f2f2; border: 1px solid #dddddd; text-align: right; padding: 8px;"><?=$total_encomendas['total']?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

<?php 
    if ($num_atendimentos != 0){
?>
    <div style="display: cell; text-align: center;">
        <h3>Atendimentos<h3>
        <div style="margin-bottom: 2%; margin-right: 1%; margin-left: 1%; border:1px solid rgba(0,0,0,.125); border-radius: .25rem;">
            <div style="width:100%;">
                <div> 
                    <table style="width:100%; border-collapse: collapse; font-family: arial, sans-serif; text-align: left">
                        <thead>
                            <tr style="background-color: #e6e6e6;">
                                <th style="text-align: center;">Horário</th>
                                <th style="text-align: center;">Tipo</th>
                                <th style="text-align: center;">Atendido</th>
                                <th style="text-align: center;">Demanda</th>
                                <th style="text-align: center;">Telefone</th>
                                <th style="text-align: center;">IIPC Net</th>
                                <th style="text-align: center;">Interesses</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php 
                            $i = 1;
                            while ($linha_atendimentos = $res_atendimentos -> fetch_assoc()){
                                if ($i == 0){
                                    ?>
                                        <tr>
                                            <td style="background-color: #f2f2f2; border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=substr(UTF8_ENCODE($linha_atendimentos['horas']), 0, -3)?></td>
                                            <td style="background-color: #f2f2f2; border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_atendimentos['tipo_atendimento'])?></td>
                                            <td style="background-color: #f2f2f2; border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_atendimentos['nome'])?></td>
                                            <td style="background-color: #f2f2f2; border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_atendimentos['descricao'])?></td>
                                            <td style="background-color: #f2f2f2; border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_atendimentos['telefone'])?></td>
                                            <td style="background-color: #f2f2f2; border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_atendimentos['codiipcnet'])?></td>
                                            <td style="background-color: #f2f2f2; border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_atendimentos['livro'])?><br/><?=UTF8_ENCODE($linha_atendimentos['evento'])?></td>         
                                        </tr>
                                    <?php
                                    $i=1;
                                }else{
                                    ?>
                                        <tr>
                                            <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=substr(UTF8_ENCODE($linha_atendimentos['horas']), 0, -3)?></td>
                                            <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_atendimentos['tipo_atendimento'])?></td>
                                            <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_atendimentos['nome'])?></td>
                                            <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_atendimentos['descricao'])?></td>
                                            <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_atendimentos['telefone'])?></td>
                                            <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_atendimentos['codiipcnet'])?></td>
                                            <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_atendimentos['livro'])?><br/><?=UTF8_ENCODE($linha_atendimentos['evento'])?></td>
                                        </tr>
                                    <?php
                                    $i=0;
                                }
                            }  
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php 
}

if ($total_livros['total']+$num_cursos != 0){
?>
    <div style="display: cell; text-align: center;">
        <h3>Vendas<h3>
        <div style="margin-bottom: 2%; margin-right: 1%; margin-left: 1%; border:1px solid rgba(0,0,0,.125); border-radius: .25rem;">
            <div>
                <table style="width:100%; border-collapse: collapse; font-family: arial, sans-serif;">
                    <thead>
                        <tr style="background-color: #e6e6e6;">
                            <th style="text-align: center;">Horário</th>
                            <th style="text-align: center;">Produto</th>
                            <th style="text-align: center;">Comprador</th>
                            <th style="text-align: center;">Valor</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
                        $i = 1;
                        while ($linha_vendas = $res_vendas -> fetch_assoc()){
                            if ($i == 0){
                                ?>
                                    <tr>
                                        <td style="background-color: #f2f2f2; border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=substr(UTF8_ENCODE($linha_vendas['horario']), 0, -3)?></td>
                                        <td style="background-color: #f2f2f2; border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_vendas['quantidade']).' x '.UTF8_ENCODE($linha_vendas['produto'])?></td>
                                        <td style="background-color: #f2f2f2; border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_vendas['nome'])?></td>
                                        <td style="background-color: #f2f2f2; border: 1px solid #dddddd; text-align: left; padding: 8px;">R$ <?=UTF8_ENCODE($linha_vendas['valor'])?> - <?=UTF8_ENCODE($linha_vendas['descricao'])?></td>
                                    </tr>
                                <?php
                                $i=1;
                            }else{
                                ?>
                                    <tr>
                                        <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=substr(UTF8_ENCODE($linha_vendas['horario']), 0, -3)?></td>
                                        <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_vendas['quantidade']).' x '.UTF8_ENCODE($linha_vendas['produto'])?></td>
                                        <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_vendas['nome'])?></td>
                                        <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;">R$ <?=UTF8_ENCODE($linha_vendas['valor'])?> - <?=UTF8_ENCODE($linha_vendas['descricao'])?></td>
                                    </tr>
                                <?php
                                $i=0;
                            }
                        }  
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<?php
}
?>