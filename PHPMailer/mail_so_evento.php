<?php
//DATA DO RELATÓRIO
date_default_timezone_set('America/Sao_Paulo');
$data = date('d/m/Y', time());

include "conectasql.php";
// ATENDIMENTOS
$atendimentos = "SELECT TIME(a.data_atendimento) as horas, ta.descricao AS tipo_atendimento, a.descricao, 
ifnull(al.NOME, asi.nome) AS nome, ifnull(al.telefone1, asi.telefone) as telefone, 
ifnull(al.CODIGO, '-') AS codiipcnet, l.nome as livro, e.NOME as evento
FROM atendimento a inner join tipo_atendimento ta on a.tipo_atendimento = ta.id
left join aluno al on al.CODIGO = a.id_aluno left join atendido_sem_iipcnet asi on asi.id = a.id_aluno
left join aluno_sem_iipcnet asii on (asii.cpf = a.id_aluno OR asii.rg = a.id_aluno)
left join livro_interesse li on li.id_relatorio_atendimento = a.id_atendimento
LEFT join livro l on l.idLivro = li.id_livro left join evento_interesse ei on ei.id_relatorio_atendimento = a.id_atendimento
left join evento e on e.id = ei.evento_id WHERE DATE(a.data_atendimento) = DATE(NOW()) order by a.data_atendimento";
$res_atendimentos = $conexao ->query($atendimentos);

$num_atendimentos = mysqli_num_rows($res_atendimentos);


//VENDAS DE LIVROS
$vendas = "SELECT TIME(v.data) AS horario,
    ifnull(al.NOME, ifnull(asi.nome, asii.nome)) AS nome,  
	ifnull(al.telefone1, ifnull(asi.telefone,asii.telefone_fixo)) as telefone,  
	ifnull(al.CODIGO, '-') AS codiipcnet, 
	ifnull(l.nome,e.NOME) as produto,
    ifnull(v.quantidade, 1) as quantidade,
    p.valor,
    tp.descricao
	from venda v inner join pagamento p on v.id_pagamento = p.id_pagamento 
    inner join tipo_pagamento tp on tp.id = p.tipo_pagamento
    left join aluno al on al.CODIGO = v.id_aluno left join atendido_sem_iipcnet asi on asi.id = v.id_aluno 
    left join aluno_sem_iipcnet asii on (asii.cpf = v.id_aluno OR asii.rg = v.id_aluno)
	LEFT join livro l on l.idLivro = v.id_livro
	left join evento e on e.id = v.id_evento
    WHERE DATE(v.data) = DATE(NOW())
	order by v.data";
$res_vendas = $conexao ->query($vendas);

$total_vendaslivros = "SELECT SUM(v.quantidade) as total FROM venda v
WHERE DATE(v.data) = CURRENT_DATE AND v.id_livro IS NOT NULL";
$res_total_vendaslivros = $conexao ->query($total_vendaslivros);
$total_livros = $res_total_vendaslivros -> fetch_assoc();
if (is_null($total_livros['total']) === true) {
    $total_livros['total'] = 0;
}


//VENDAS DE CURSOS
$vendascursos = "SELECT SUM(v.quantidade) as total FROM venda v
WHERE DATE(v.data) = CURRENT_DATE AND v.id_evento IS NOT NULL";
$res_vendascursos = $conexao ->query($vendascursos);
$total_cursos = $res_vendascursos -> fetch_assoc();
if (is_null($total_cursos['total']) === true) {
    $total_cursos['total'] = 0;
}

//CORRESPONDENCIAS
$encomendas = "SELECT COUNT(*) AS total FROM `encomenda` WHERE DATE(`data`) = DATE(NOW())";
$res_encomendas = $conexao -> query($encomendas);
$total_encomendas = $res_encomendas -> fetch_assoc();

//TENEPES
$tenepes = "SELECT COUNT(*) as total FROM `tenepes` WHERE DATE(`data`) = DATE(NOW())";
$res_tenepes = $conexao -> query($tenepes);
$total_tenepes = $res_tenepes -> fetch_assoc();

//HORAS TMK TOTAL
$horas_tmk = "SELECT LEFT(SEC_TO_TIME(SUM((UNIX_TIMESTAMP(termino) - UNIX_TIMESTAMP(inicio)))),5) 
as total FROM sessaotmk WHERE DATE(`inicio`) = DATE(NOW())";
$res_horas_tmk = $conexao -> query($horas_tmk);
$total_horas_tmk = $res_horas_tmk -> fetch_assoc();
if (is_null($total_horas_tmk['total']) === true) {
    $total_horas_tmk['total'] = "00:00";
}

//LIGACOES TMK TOTAL
$ligacoes_tmk = "SELECT COUNT(*) as total FROM ligacoes_tmk l WHERE DATE(l.data) = DATE(NOW())";
$res_ligacoes_tmk = $conexao -> query($ligacoes_tmk);
$total_ligacoes_tmk = $res_ligacoes_tmk -> fetch_assoc();



//HORAS/LIGACOES TMK POR CURSO
$tmk_porcurso = "SELECT total.evento, LEFT(SEC_TO_TIME(SUM(total.duracao)),5) as tempo, SUM(total.ligacoes) as ligacoess FROM
(SELECT s.id as sessao, ((UNIX_TIMESTAMP(s.termino) - UNIX_TIMESTAMP(s.inicio)))*((COUNT(l.id))/((SELECT COUNT(l2.id) FROM
ligacoes_tmk l2 WHERE l2.sessaotmk_id = s.id))) as duracao, e.NOME as
evento, COUNT(l.id) as ligacoes FROM sessaotmk s INNER JOIN ligacoes_tmk l ON l.sessaotmk_id = s.id INNER JOIN evento e ON e.id = l.evento_id
WHERE DATE(s.inicio) = DATE(NOW()) GROUP BY evento, sessao) total GROUP BY evento";
$res_tmk_porcurso = $conexao -> query($tmk_porcurso);

//NÚMERO DE EVENTOS GRATUITOS
$eventos = "SELECT tg.id_turma, tg.nome, COUNT(tga.id_aluno) as aluno FROM turma_gratuita tg INNER JOIN turma t ON t.CODIGO = tg.id_turma
AND DATE(t.DATA_INICIAL) = DATE(NOW()) INNER JOIN turma_gratuita_aluno tga ON tga.id_turma = tg.id_turma GROUP BY tg.nome";
$res_eventos = $conexao -> query($eventos);
$total_eventos = mysqli_num_rows($res_eventos);

//NÚMERO DE ALUNOS EM EVENTOS GRATUITOS

//LISTA DE ALUNOS POR EVENTO
$aluno_curso = "SELECT tg.nome, IFNULL(a.NOME,asi.nome) as aluno, o.descricao as origem,
IF(tga.fl_primeira_vez=1,'Sim','Nao') as primeira_vez FROM turma_gratuita tg
INNER JOIN turma t ON t.CODIGO = tg.id_turma AND DATE(t.DATA_INICIAL) = DATE(NOW())
INNER JOIN turma_gratuita_aluno tga ON tga.id_turma = tg.id_turma LEFT JOIN aluno a ON a.CODIGO = tga.id_aluno
LEFT JOIN aluno_sem_iipcnet asi ON (asi.rg = tga.id_aluno OR asi.cpf = tga.id_aluno) INNER JOIN tipo_origem o ON o.id = tga.tp_origem
ORDER BY tg.nome";
$res_aluno_curso = $conexao -> query($aluno_curso);
 


ob_start();

?>
<div style="display: table">
<div style="text-align: center; display: cell;">
    <h2>Relatório Diário - <?=$data?></h2>
</div>

<div style="display: cell; text-align: center;">
    <h3>Resumo do Atendimento<h3>
    <div style="margin-bottom: 2%; margin-right: 1%; margin-left: 1%; border:2px solid #8d8d8d; border-radius: .25rem; text-align: center;">
        <table style="width:100%; border-collapse: collapse; font-family: arial, sans-serif; text-align: left">
            <tbody>
                <tr>
                    <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;">Atendimentos</td>
                    <td style="border: 1px solid #dddddd; text-align: right; padding: 8px;"><?=$num_atendimentos?></td>
                </tr>
                <tr>
                    <td style="background-color: #f2f2f2; border: 1px solid #dddddd; text-align: left; padding: 8px;">Vendas</td>
                    <td style="background-color: #f2f2f2; border: 1px solid #dddddd; text-align: right; padding: 8px;"><?=$total_livros['total']+$total_cursos['total']?></td>
                </tr>
                <tr>
                    <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;">Pedido de Tenepes</td>
                    <td style="border: 1px solid #dddddd; text-align: right; padding: 8px;"><?=$total_tenepes['total']?></td>
                </tr>
                <tr>
                    <td style="background-color: #f2f2f2; border: 1px solid #dddddd; text-align: left; padding: 8px;">Correspondências Recebidas</td>
                    <td style="background-color: #f2f2f2; border: 1px solid #dddddd; text-align: right; padding: 8px;"><?=$total_encomendas['total']?></td>
                </tr>
                <tr>
                    <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;">Horas de TMK</td>
                    <td style="border: 1px solid #dddddd; text-align: right; padding: 8px;"><?=$total_horas_tmk['total']?></td>
                </tr>
                <tr>
                    <td style="background-color: #f2f2f2; border: 1px solid #dddddd; text-align: left; padding: 8px;">Ligações no TMK</td>
                    <td style="background-color: #f2f2f2; border: 1px solid #dddddd; text-align: right; padding: 8px;"><?=$total_ligacoes_tmk['total']?></td>
                </tr>
                <tr>
                    <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;">Eventos Gratuitos</td>
                    <td style="border: 1px solid #dddddd; text-align: right; padding: 8px;"><?=$total_eventos?></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<?php 
if (1 == 0){
?>
<div style="display: cell; text-align: center;">
    <h3>Atendimentos<h3>
    <div style="margin-bottom: 2%; margin-right: 1%; margin-left: 1%; border:1px solid rgba(0,0,0,.125); border-radius: .25rem;">
        <div style="width:100%;">
            <div> 
                <table style="width:100%; border-collapse: collapse; font-family: arial, sans-serif; text-align: left">
                    <thead>
                        <tr style="background-color: #de6a67;">
                            <th style="text-align: center;">Horário</th>
                            <th style="text-align: center;">Tipo</th>
                            <th style="text-align: center;">Atendido</th>
                            <th style="text-align: center;">Demanda</th>
                            <th style="text-align: center;">Telefone</th>
                            <th style="text-align: center;">IIPC Net</th>
                            <th style="text-align: center;">Interesses</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
                        $i = 1;
                        while ($linha_atendimentos = $res_atendimentos -> fetch_assoc()){
                            if ($i == 0){
                                ?>
                                    <tr>
                                        <td style="background-color: #fcefef; border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=substr(UTF8_ENCODE($linha_atendimentos['horas']), 0, -3)?></td>
                                        <td style="background-color: #fcefef; border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_atendimentos['tipo_atendimento'])?></td>
                                        <td style="background-color: #fcefef; border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_atendimentos['nome'])?></td>
                                        <td style="background-color: #fcefef; border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_atendimentos['descricao'])?></td>
                                        <td style="background-color: #fcefef; border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_atendimentos['telefone'])?></td>
                                        <td style="background-color: #fcefef; border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_atendimentos['codiipcnet'])?></td>
                                        <td style="background-color: #fcefef; border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_atendimentos['livro'])?><br/><?=UTF8_ENCODE($linha_atendimentos['evento'])?></td>         
                                    </tr>
                                <?php
                                $i=1;
                            }else{
                                ?>
                                    <tr>
                                        <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=substr(UTF8_ENCODE($linha_atendimentos['horas']), 0, -3)?></td>
                                        <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_atendimentos['tipo_atendimento'])?></td>
                                        <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_atendimentos['nome'])?></td>
                                        <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_atendimentos['descricao'])?></td>
                                        <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_atendimentos['telefone'])?></td>
                                        <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_atendimentos['codiipcnet'])?></td>
                                        <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_atendimentos['livro'])?><br/><?=UTF8_ENCODE($linha_atendimentos['evento'])?></td>
                                    </tr>
                                <?php
                                $i=0;
                            }
                        }  
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php 
}

if (1 == 0){
?>
<div style="display: cell; text-align: center;">
    <h3>Vendas<h3>
    <div style="margin-bottom: 2%; margin-right: 1%; margin-left: 1%; border:1px solid rgba(0,0,0,.125); border-radius: .25rem;">
        <div>
            <table style="width:100%; border-collapse: collapse; font-family: arial, sans-serif;">
                <thead>
                    <tr style="background-color: #7ac57a;">
                        <th style="text-align: center;">Horário</th>
                        <th style="text-align: center;">Produto</th>
                        <th style="text-align: center;">Comprador</th>
                        <th style="text-align: center;">Valor</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
                    $i = 1;
                    while ($linha_vendas = $res_vendas -> fetch_assoc()){
                        if ($i == 0){
                            ?>
                                <tr>
                                    <td style="background-color: #f2f9f2; border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=substr(UTF8_ENCODE($linha_vendas['horario']), 0, -3)?></td>
                                    <td style="background-color: #f2f9f2; border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_vendas['quantidade']).' x '.UTF8_ENCODE($linha_vendas['produto'])?></td>
                                    <td style="background-color: #f2f9f2; border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_vendas['nome'])?></td>
                                    <td style="background-color: #f2f9f2; border: 1px solid #dddddd; text-align: left; padding: 8px;">R$ <?=UTF8_ENCODE($linha_vendas['valor'])?> / <?=UTF8_ENCODE($linha_vendas['descricao'])?></td>
                                </tr>
                            <?php
                            $i=1;
                        }else{
                            ?>
                                <tr>
                                    <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=substr(UTF8_ENCODE($linha_vendas['horario']), 0, -3)?></td>
                                    <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_vendas['quantidade']).' x '.UTF8_ENCODE($linha_vendas['produto'])?></td>
                                    <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_vendas['nome'])?></td>
                                    <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;">R$ <?=UTF8_ENCODE($linha_vendas['valor'])?> / <?=UTF8_ENCODE($linha_vendas['descricao'])?></td>
                                </tr>
                            <?php
                            $i=0;
                        }
                    }  
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php
}

if (1 == 0){
?>
<div style="display: cell; text-align: center;">
    <h3>Ligações TMK<h3>
    <div style="margin-bottom: 2%; margin-right: 1%; margin-left: 1%; border:1px solid rgba(0,0,0,.125); border-radius: .25rem;">
        <div style="width:100%;">
            <div> 
                <table style="width:100%; border-collapse: collapse; font-family: arial, sans-serif; text-align: left">
                    <thead>
                        <tr style="background-color: #f3be76;">
                            <th style="text-align: center;">Curso</th>
                            <th style="text-align: center;">Número de Ligações</th>
                            <th style="text-align: center;">Tempo</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
                        $i = 1;
                        while ($linha_ligacoes = $res_tmk_porcurso -> fetch_assoc()){
                            if ($i == 0){
                                ?>
                                    <tr>
                                        <td style="background-color: #fff2ea; border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_ligacoes['evento'])?></td>
                                        <td style="background-color: #fff2ea; border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_ligacoes['ligacoess'])?></td>
                                        <td style="background-color: #fff2ea; border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_ligacoes['tempo'])?></td>
                                    </tr>
                                <?php
                                $i=1;
                            }else{
                                ?>
                                    <tr>
                                        <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_ligacoes['evento'])?></td>
                                        <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_ligacoes['ligacoess'])?></td>
                                        <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_ligacoes['tempo'])?></td>
                                    </tr>
                                <?php
                                $i=0;
                            }
                        }  
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php 
}

if ($total_eventos > 0){
?>
<div style="display: cell; text-align: center;">
    <h3>Eventos gratuitos<h3>
    <div style="margin-bottom: 2%; margin-right: 1%; margin-left: 1%; border:1px solid rgba(0,0,0,.125); border-radius: .25rem;">
        <div style="width:100%;">
            <?php
            for ($c = 1; $c <= $total_eventos; $c++) {?>
            <div style="margin-bottom: 2%; margin-right: 1%; margin-left: 1%; border:2px solid #17a2b8; border-radius: .25rem;">
                <div style="width:100%;">
                    <div>
                        <?php
                            $curso = $res_eventos->fetch_assoc();
                            $evento_especifico = "SELECT o.descricao as origem, SUM(IF(tga.fl_primeira_vez=0,1,0)) as total, SUM(IF(tga.fl_primeira_vez=1,1,0))
                            as primeira_vez FROM turma_gratuita tg INNER JOIN turma_gratuita_aluno tga ON tga.id_turma = tg.id_turma INNER JOIN
                            tipo_origem o ON o.id = tga.tp_origem WHERE tg.id_turma = ".$curso['id_turma']." GROUP BY origem ORDER BY origem";
                            $res_evento_especifico = $conexao -> query($evento_especifico);
                        ?>
                        <table style="width:100%; border-collapse: collapse; font-family: arial, sans-serif; text-align: left;">
                            <tbody>
                                <tr>
                                    <th style="background-color: #17a2b8; text-align: center;"><font color = "white"><?php echo(UTF8_ENCODE($curso['nome']." - ".$curso['aluno']." alunos")); ?></font></th>
                                </tr>
                            </tbody>
                        </table>
                        <table style="width:100%; border-collapse: collapse; font-family: arial, sans-serif; text-align: left;">
                            <thead>
                                <tr style="background-color: #99d9ea;">
                                    <th style="text-align: center;"><?php echo("Origem"); ?></th>
                                    <th style="text-align: center;"><?php echo("Total"); ?></th>
                                    <th style="text-align: center;"><?php echo("1ª Vez"); ?></th>
                                    <th style="text-align: center;"><?php echo("Frequentes"); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                            $i = 1;
                            $total_primeira_vez = 0;
                            $total_frequente = 0;
                            while ($linha_evento = $res_evento_especifico -> fetch_assoc()){
                                $total_primeira_vez = $total_primeira_vez + $linha_evento['primeira_vez'];
                                $total_frequente = $total_frequente + $linha_evento['total'];
                                if ($i == 0){
                                    ?>
                                        <tr>
                                            <td style="background-color: #edf9fc; border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_evento['origem'])?></td>
                                            <td style="background-color: #edf9fc; border: 1px solid #dddddd; text-align: center; padding: 8px;"><?=UTF8_ENCODE($linha_evento['primeira_vez']+$linha_evento['total'])?></td>
                                            <td style="background-color: #edf9fc; border: 1px solid #dddddd; text-align: center; padding: 8px;"><?=UTF8_ENCODE($linha_evento['primeira_vez'])?></td>
                                            <td style="background-color: #edf9fc; border: 1px solid #dddddd; text-align: center; padding: 8px;"><?=UTF8_ENCODE($linha_evento['total'])?></td>
                                        </tr>
                                    <?php
                                    $i=1;
                                }else{
                                    ?>
                                        <tr>
                                            <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_evento['origem'])?></td>
                                            <td style="border: 1px solid #dddddd; text-align: center; padding: 8px;"><?=UTF8_ENCODE($linha_evento['primeira_vez']+$linha_evento['total'])?></td>
                                            <td style="border: 1px solid #dddddd; text-align: center; padding: 8px;"><?=UTF8_ENCODE($linha_evento['primeira_vez'])?></td>
                                            <td style="border: 1px solid #dddddd; text-align: center; padding: 8px;"><?=UTF8_ENCODE($linha_evento['total'])?></td>
                                        </tr>
                                    <?php
                                    $i=0;
                                }
                            }
                            if ($i == 0){
                                ?>
                                    <tr>
                                        <td style="background-color: #edf9fc; border: 1px solid #dddddd; text-align: left; padding: 8px;"><b>TOTAL</b></td>
                                        <td style="background-color: #edf9fc; border: 1px solid #dddddd; text-align: center; padding: 8px;"><b><?=UTF8_ENCODE($total_primeira_vez+$total_frequente)?></b></td>
                                        <td style="background-color: #edf9fc; border: 1px solid #dddddd; text-align: center; padding: 8px;"><b><?=UTF8_ENCODE($total_primeira_vez)?></b></td>
                                        <td style="background-color: #edf9fc; border: 1px solid #dddddd; text-align: center; padding: 8px;"><b><?=UTF8_ENCODE($total_frequente)?></b></td>
                                    </tr>
                                <?php
                                $i=1;
                            }else{
                                ?>
                                    <tr>
                                        <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;"><b>TOTAL</b></td>
                                        <td style="border: 1px solid #dddddd; text-align: center; padding: 8px;"><b><?=UTF8_ENCODE($total_primeira_vez+$total_frequente)?></b></td>
                                        <td style="border: 1px solid #dddddd; text-align: center; padding: 8px;"><b><?=UTF8_ENCODE($total_primeira_vez)?></b></td>
                                        <td style="border: 1px solid #dddddd; text-align: center; padding: 8px;"><b><?=UTF8_ENCODE($total_frequente)?></b></td>
                                    </tr>
                                <?php
                                $i=0;
                            }
                            ?>
                            </tbody>
                        </table><br>
                        <table style="width:100%; border-collapse: collapse; font-family: arial, sans-serif; text-align: left;">
                            <thead>
                                <tr style="background-color: #99d9ea;">
                                    <th style="text-align: center;">Aluno</th>
                                    <th style="text-align: center;">Origem</th>
                                    <th style="text-align: center;">Primeira Vez?</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                                if ($c != 1){
                                    ?>
                                    <tr>
                                        <td style="background-color: #edf9fc; border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_curso['aluno'])?></td>
                                        <td style="background-color: #edf9fc; border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_curso['origem'])?></td>
                                        <td style="background-color: #edf9fc; border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_curso['primeira_vez'])?></td>
                                    </tr>
                                    <?php
                                }
                                $i = 1;
                                while ($linha_curso = $res_aluno_curso -> fetch_assoc()){
                                    if($curso['nome'] != $linha_curso['nome']) break;
                                    if ($i == 0){
                                        ?>
                                            <tr>
                                                <td style="background-color: #edf9fc; border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_curso['aluno'])?></td>
                                                <td style="background-color: #edf9fc; border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_curso['origem'])?></td>
                                                <td style="background-color: #edf9fc; border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_curso['primeira_vez'])?></td>
                                            </tr>
                                        <?php
                                        $i=1;
                                    }else{
                                        ?>
                                            <tr>
                                                <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_curso['aluno'])?></td>
                                                <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_curso['origem'])?></td>
                                                <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;"><?=UTF8_ENCODE($linha_curso['primeira_vez'])?></td>
                                            </tr>
                                        <?php
                                        $i=0;
                                    }
                                }  
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</div>
<?php 
}
?>
<div style="display: cell; text-align: center;">
    <font size="4"><b>Esqueceu algum lançamento? Faça-o <a href="http://mediatrizlab.com/iipc/relatorio_atendimento/">aqui</a>! A sua contribuição é muito importante!!</b></font>
</div>
</div>
<?php

$body = ob_get_clean();

echo($body);

require 'PHPMailerAutoload.php';
$mail = new PHPMailer;
$mail->setFrom('ti.iipcbh@gmail.com', 'Atendimento IIPC-BH');
$mail->addAddress('voluntariosiipcbh@googlegroups.com', 'Voluntarios');
$mail->Subject  = 'Relatorio de Atendimento';
$mail->isHTML(true);
$mail->Body= $body;
if(!$mail->send()) {
  echo 'Message was not sent.';
  echo 'Mailer error: ' . $mail->ErrorInfo;
} else {
  echo 'Message has been sent.';
}
?>