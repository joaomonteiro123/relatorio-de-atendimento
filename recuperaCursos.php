<?php
include "conectasql.php";
session_start();
mysqli_set_charset($conexao,"utf8");

$sql = "SELECT e.NOME as nome_curso, DATE_FORMAT(v.data, '%H:%i') as hora, CASE WHEN v.fl_sem_iipcnet = 1 THEN asi.nome ELSE a.NOME END AS 'nome_comprador', p.valor
FROM 
venda v inner join evento e on v.id_evento = e.id
left join aluno a on a.CODIGO=v.id_aluno
LEFT join atendido_sem_iipcnet asi on asi.id = v.id_aluno
inner join pagamento p on p.id_pagamento = v.id_pagamento
WHERE date(v.data) = curdate()
order by v.data desc"; 

$res_cursos = $conexao ->query($sql);

$data = array('');
while ($linha = $res_cursos -> fetch_assoc()){
   array_push($data,$linha);
}

echo json_encode($data,JSON_UNESCAPED_UNICODE);