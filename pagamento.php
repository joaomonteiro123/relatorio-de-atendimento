<script>


  $('#credito').click(function () {
    if ($(this).is(':checked')) {
      $('#autorizacao').attr("required");
      $('#bandeira').attr("required");
      $('#parcelas').attr("required");
      $("#div_cartao").show();
    }
  });
  $('#debito').click(function () {
    if ($(this).is(':checked')) {
      $('#autorizacao').attr("required");
      $('#bandeira').attr("required");
      $('#parcelas').attr("required");
      $("#div_cartao").show();
    }
  });
  $('#dinheiro').click(function () {
    if ($(this).is(':checked')) {
      $('#autorizacao').removeAttr("required");
      $('#bandeira').removeAttr("required");
      $('#parcelas').removeAttr("required");
      $("#div_cartao").hide();
    }
  });
  $('#cheque').click(function () {
    if ($(this).is(':checked')) {
      $('#autorizacao').removeAttr("required");
      $('#bandeira').removeAttr("required");
      $('#parcelas').removeAttr("required");
      $("#div_cartao").hide();
    }
  });


</script>


<div class="row">
    <div class="col-md-6 mb-1">
      <div class="d-block my-3">
        <div class="custom-control custom-radio">
          <input id="dinheiro" name="tipo_pagamento" value="1" type="radio" class="custom-control-input" required>
          <label class="custom-control-label" for="dinheiro">Dinheiro</label>
        </div>

        <div class="custom-control custom-radio">
          <input id="debito" name="tipo_pagamento" value="3" type="radio" class="custom-control-input" required>
          <label class="custom-control-label" for="debito">Débito</label>
        </div>
        <div class="custom-control custom-radio">
          <input id="credito" name="tipo_pagamento" value="2" type="radio" class="custom-control-input" required>
          <label class="custom-control-label" for="credito">Crédito</label>
        </div>
        <div class="custom-control custom-radio">
          <input id="cheque" name="tipo_pagamento" value="4" type="radio" class="custom-control-input" required>
          <label class="custom-control-label" for="cheque">Cheque</label>
        </div>  
      </div>
    </div>
    <div class="col-md-6 mb-1">
    <label for="valor">Valor da Compra</label>
    <div class="input-group mb-3">
      <div class="input-group-prepend">
        <span class="input-group-text">R$</span>
      </div>
      <input type="text" style="width:40%" onkeyup="setDecimalValue()" id="valor" name="valor" placeholder="" value="" required=""/>
    </div>
    </div>
</div>
<div class="row" id="div_cartao" style="display: none;">
  <div class="row">
    <div class="col-md-4 mb-3">
      <label for="cc-name">Autorização Nº</label>
      <input type="text" class="form-control" id="autorizacao" name="autorizacao" placeholder="" >
      <div class="invalid-feedback">
        Favor informar o nº da autorização.
      </div>
    </div>
    <div class="col-md-4 mb-3">
      <label for="cc-number">Bandeira</label>
      <input type="text" class="form-control" id="bandeira" name="bandeira" placeholder="" >
      <small class="text-muted">Ex: Visa, Master</small>
      <div class="invalid-feedback">
        Favor informar a bandeira do cartão
      </div>
    </div>
    <div class="col-md-2 mb-3">
      <label for="cc-expiration">Parcelas</label>
      <input type="number" max="12" class="form-control" value="1" id="parcelas" name="parcelas" placeholder="" >
      <div class="invalid-feedback">
        Favor informar o número de parcelas
      </div>
    </div> 
  </div>         
</div>