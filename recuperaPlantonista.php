<?php
include "conectasql.php";
session_start();
mysqli_set_charset($conexao,"utf8");

$sql = "SELECT p.*, v.nome FROM plantonista p INNER JOIN voluntario v ON v.codigo = p.id_voluntario WHERE date(p.data_plantao) = curdate()"; 
$res_plantonistas = $conexao ->query($sql);

$data = array('');
while ($linha = $res_plantonistas -> fetch_assoc()){
   array_push($data,$linha);
}

echo json_encode($data,JSON_UNESCAPED_UNICODE);