<?php

include "conectasql.php";

$nomes_filtro = "(SELECT CODIGO, NOME, 0 AS fl_sem_iipcnet FROM `aluno` WHERE UF = 'MG' AND NOME LIKE '".trim(strip_tags(utf8_decode($_GET['term'])))."%' ORDER BY NOME) UNION (SELECT id AS CODIGO, nome AS NOME, 1 AS fl_sem_iipcnet FROM `atendido_sem_iipcnet` WHERE NOME LIKE '".trim(strip_tags(utf8_decode($_GET['term'])))."%' ORDER BY NOME) LIMIT 10";

$res_nomes = $conexao ->query($nomes_filtro);
$nomes = array();

$i = 0;
while ($n = $res_nomes -> fetch_assoc()) {
    $nomes[$i]["id"] = utf8_encode($n['CODIGO']);
    $nomes[$i]["label"] = utf8_encode($n['NOME']);
    $nomes[$i]["value"] = utf8_encode($n['NOME']);
    $nomes[$i]["fl_sem_iipcnet"] = utf8_encode($n['fl_sem_iipcnet']);
    $i = $i + 1;
}

//foreach($nomes['NOME'] as $nomes) {
//    echo $nomes['NOME'], '<br>';
//}

echo json_encode($nomes);

?>