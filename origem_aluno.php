<?php

include "conectasql.php";

$origens_filtro = "SELECT DISTINCT o.* FROM tipo_origem o ORDER BY descricao";
$res_origem = $conexao -> query($origens_filtro);

?>

<div class="row mb-5">
    <div class="col-md-12">
      <label for="origem">Como o aluno ficou sabendo?</label> 
      <select class="custom-select d-block w-100" name="origem" id="origem" required="">
        <option value="">Escolha...</option>
        <?php 
          while ($linha_origem = $res_origem -> fetch_assoc()){
            ?>
            <option value="<?=$linha_origem['id']?>"><?=utf8_encode($linha_origem['descricao'])?></option>  
            <?php 
          }  
        ?>
      </select>
    </div>
  </div>