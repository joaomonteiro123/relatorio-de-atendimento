<?php
include "conectasql.php";
session_start();

$aluno_codigo = $_POST['cod_iipcnet'];
$tipo_atendimento = $_POST['tipo_atendimento'];
$fl_sem_iipcnet = $_POST['fl_sem_iipcnet'];
$fl_novo_aluno = 0;
//se o aluno não possui código iipcnet
if($fl_sem_iipcnet == 0){
	$fl_novo_aluno = 1;
	include "enviaAtendidoSemIipcnet.php";	
}

	$demanda = utf8_decode($_POST['demanda']);
	$origem = (int)$_POST['origem'];
	$tipo_atendimento = (int)$tipo_atendimento;	

		
	$sql = $conexao->prepare("INSERT INTO atendimento (tipo_atendimento, id_aluno, id_origem,
	descricao, data_atendimento, fl_sem_iipcnet)
			VALUES (?, ?, ?, ?, now(), ?)");  

	$sql ->bind_param("iiisi",$tipo_atendimento,$aluno_codigo,$origem,$_POST["demanda"],$fl_sem_iipcnet); 
	$res = $sql->execute();
	$sql->close();

	$id_ra = mysqli_insert_id($conexao);
	mysqli_commit($conexao);
	if($fl_novo_aluno){
		if(is_numeric($id_aluno_banco) && $id_ra > 0){
			$sqlUpdate = mysqli_query($conexao, "UPDATE atendido_sem_iipcnet set id_atendimento = '$id_ra' where id = '$id_aluno_banco'");
		}	
	}

$i = 0;
if(isset($_POST['interessado_curso'])){ 
	while ($i < count($_POST["select_interesse_curso"])){
		$sql = $conexao->prepare("INSERT INTO evento_interesse (evento_id, aluno_codigo, data_alteracao,
		id_relatorio_atendimento)
				VALUES (?, ?, now(), ?)"); 
		$sql -> bind_param("isi",$_POST["select_interesse_curso"][$i],$aluno_codigo,$id_ra);
		$i++;
		$sql->execute();	    
	}
}

$i=0;
if(isset($_POST['interessado_livro'])){
	while ($i < count($_POST["select_interesse_livro"])){
		$sql = $conexao->prepare("INSERT INTO livro_interesse (id_livro, id_aluno, data_alteracao,
		id_relatorio_atendimento)
				VALUES (?, ?, now(), ?)");  
		$sql -> bind_param("isi",$_POST["select_interesse_livro"][$i],$aluno_codigo,$id_ra);
		$i++;
		$sql->execute();	    
	}
}
