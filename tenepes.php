<script src="jquery-3.3.1.min.js" crossorigin="anonymous"></script>

<script>
  $('form').on('submit', function (e) {

    e.preventDefault(); //prevent to reload the page

    $.ajax({
      type: 'POST', //hide url
      url: 'enviaTenepes.php', //your form validation url
      data: $('form').serialize(),
      success: function () {
        alert('Enviado com sucesso!'); //display an alert whether the form is submitted okay
        location.reload();
      }
    });
  });
</script>

<form name="frm_tenepes">
  <div class="row">
    <div class="col-md-12">
      <h4 class="">Pedido de tenepes</h4>
    </div>
  </div>
  <div class="row">
   <div class="col-md-12 md-4">
      <label for="atendido">Nome completo do atendido</label>
      <input type="text" class="form-control" id="atendido" name="atendido" placeholder="" value="" required="">
    </div>
  </div>
  <div class="row">
    <div class="col-md-12 mb-3" <label for="demanda">Endereço</label>
      <input type="text" class="form-control input-lg" id="endereco" name="endereco" placeholder="" value="" required="">
    </div>
  </div>
  <div class="row">
    <div class="col-md-12 mb-3" <label for="demanda">Motivo</label>
    <textarea style="margin-top: 10px;" type="text" class="form-control input-lg" id="motivo" name="motivo" placeholder="" value="" required="" rows="5" style="overflow:hidden"></textarea>
    </div>
  </div>
  <div class="row">
    <button class="btn btn-primary btn-lg btn-block" type="submit">Enviar</button>
  </div>
  <script src="popper.min.js" crossorigin="anonymous"></script>
  <script src="bootstrap-4.0.0-beta.3/dist/js/bootstrap.min.js" crossorigin="anonymous"></script>
</form>
