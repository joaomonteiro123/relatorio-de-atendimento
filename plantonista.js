//$("#botao-sync").click(sincronizaPlantonista);

function inserePlantonista(codigo_voluntario, nome_voluntario) {
    var corpoTabela = $(".plantonista").find("tbody");
    var linha = novaLinha(codigo_voluntario, nome_voluntario);
    linha.find(".botao-remover").click(removeLinha);
    corpoTabela.append(linha);
    $(".plantonista").slideDown(500);
    scrollPlantonista();
}

function scrollPlantonista() {
    var posicaoPlantonista = $(".plantonista").offset().top;
    $("body").animate(
    {
        scrollTop: posicaoPlantonista + "px"
    }, 1000);
}

function novaLinha(codigo_voluntario, nome_voluntario) {
    var linha = $("<tr>");
    var colunaCodigo = $("<td>").text(codigo_voluntario);
    var colunaUsuario = $("<td>").text(nome_voluntario);
    var colunaRemover = $("<td>");

    var link = $("<a>").addClass("botao-remover").attr("href", "#");
    var icone = $("<i>").addClass("small").addClass("material-icons").text("remover");

    link.append(icone);

    colunaRemover.append(link);

    linha.append(colunaCodigo);
    linha.append(colunaUsuario);
    linha.append(colunaRemover);

    return linha;
}

function removeLinha(event, pdiv) {
    event.preventDefault();
  
    var linha = $(this).parent().parent();
    var voluntario = $(this).parent().parent().find("td:nth-child(1)").text();
    var dados = {
        voluntario: voluntario
    };
    $.post("removePlantonista.php", dados, function(){
    });
    linha.fadeOut(1000);
    setTimeout(function() {
        linha.remove();
    }, 1000);
}

function sincronizaPlantonista(){

    var plantonista = [];
    var linhas = $("tbody>tr");

    linhas.each(function(){
        var usuario = $(this).find("td:nth-child(1)").text();

        var score = {
            usuario: usuario           
        };

        plantonista.push(score);

        var dados = {
            plantonista: plantonista
        };

        $.post("http://localhost:3000/plantonista", dados , function() {
            console.log("Plantonista sincronizado com sucesso");
            $(".tooltip").tooltipster("open"); 
        }).fail(function(){
            $(".tooltip").tooltipster("open").tooltipster("content", "Falha ao sincronizar"); 
        }).always(function(){ //novo
            setTimeout(function() {
            $(".tooltip").tooltipster("close"); 
        }, 1200);
});

    });
}


function atualizaPlantonista(scroll){
    $.get("recuperaPlantonista.php",function(data){
        var obj =  JSON.parse(data);
        for(i=1; i<obj.length; i++){
            console.log(obj[i].id_voluntario);
            var linha = novaLinha(obj[i].id_voluntario,obj[i].nome);

            linha.find(".botao-remover").click(removeLinha);

            var corpoTabela = $(".plantonista").find("tbody");
            corpoTabela.append(linha);
            $(".plantonista").slideDown(500);
            
        };
        if(scroll == 1){
            scrollPlantonista();
        }
    });
}