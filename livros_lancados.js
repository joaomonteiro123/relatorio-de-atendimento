//$("#botao-sync").click(sincronizaPlantonista);

function insereCursos(nome_livro, nome_comprador,valor) {
    var corpoTabela = $(".livros_lancados").find("tbody");
    var linha = novaLinha(nome_livro, nome_comprador,valor);
    corpoTabela.prepend(linha);
    $(".livros_lancados").slideDown(500);
    scrollCursosLancados();
}

function scrollCursosLancados() {
    var posicaoLivros = $(".livros_lancados").offset().top;
    $("body").animate(
    {
        scrollTop: posicaoLivros + "px"
    }, 200);
}

function novaLinha(nome_livro, nome_comprador, valor) {
    var linha = $("<tr>");
    var colunaCodigo = $("<td>").text(nome_livro);
    var colunaUsuario = $("<td>").text(nome_comprador);
    var colunaValor = $("<td>").text(valor);

    linha.append(colunaCodigo);
    linha.append(colunaUsuario);
    linha.append(colunaValor);

    return linha;
}


function sincronizaCursosLancados(){

    var livros = [];
    var linhas = $("tbody>tr");

    linhas.each(function(){
        var usuario = $(this).find("td:nth-child(1)").text();

        var score = {
            usuario: usuario           
        };

        livros.push(score);

        var dados = {
            livros: livros
        };

        $.post("http://localhost:3000/livros_lancados", dados , function() {
            console.log("Livros sincronizado com sucesso");
            $(".tooltip").tooltipster("open"); 
        }).fail(function(){
            $(".tooltip").tooltipster("open").tooltipster("content", "Falha ao sincronizar"); 
        }).always(function(){ //novo
            setTimeout(function() {
            $(".tooltip").tooltipster("close"); 
        }, 1200);
});

    });
}


function atualizaCursos(scroll){
    $.get("recuperaLivros.php",function(data){
        var obj =  JSON.parse(data);
        for(i=1; i<obj.length; i++){
            console.log(obj[i].nome_livro,obj[i].nome_comprador,obj[i].valor);
            var linha = novaLinha(obj[i].nome_livro,obj[i].nome_comprador,obj[i].valor);
            var corpoTabela = $(".livros_lancados").find("tbody");
            corpoTabela.append(linha);
            $(".livros_lancados").slideDown(500);        
        };
        if(scroll == 1){
            scrollCursosLancados();
        }
    });
}