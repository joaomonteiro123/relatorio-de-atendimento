<?php

include "conectasql.php";

?>

<link rel="stylesheet" href="estilo.css">

<script>

  $(document).ready(function () {  
    var scroll = (<?php echo $_GET['scroll'] ?>);  
    atualizaVendas(scroll);
  });

</script>

<form name="frm_vendas_lancadas">    
  <section class="vendas_lancadas">
  <br/><br/>
          <h6 class="center">Vendidos hoje:</h3>

          <div class="bd-example" style="background-color:#ffffff">
            <table class="table centered bordered table-striped">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Comprador</th>
                        <th>Valor</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </section>
        </section> 
</form>