<?php

include "conectasql.php";

$voluntario_filtro = "SELECT DISTINCT v.* 
FROM voluntario v 
WHERE NOT EXISTS (SELECT 1
                  FROM plantonista p
                  WHERE date(p.data_plantao) = curdate()
                  AND p.id_voluntario = v.codigo)
ORDER BY NOME";
$res_voluntario = $conexao ->query($voluntario_filtro);

?>

<script src="jquery-3.3.1.min.js" crossorigin="anonymous"></script>
<link rel="stylesheet" href="estilo.css">

<script>

  $(document).ready(function () {  
    var scroll = (<?php echo $_GET['scroll'] ?>);  
    atualizaPlantonista(scroll);
  });


  $('form').on('submit', function (e) {
    e.preventDefault(); //prevent to reload the page
    $.ajax({
      type: 'POST', //hide url
      url: 'enviaPlantonista.php', //your form validation url
      data: $('frm_plantonista').serialize()+"&voluntario="+$("#voluntarios").val(),
      success: function () {
        var sel = document.getElementById("voluntarios");
        var nome_voluntario = sel.options[sel.selectedIndex].text;
        inserePlantonista($("#voluntarios").val(),nome_voluntario);
        $("#voluntarios").find('option:selected').remove();
      }
    });
  });
</script>

<form name="frm_plantonista">
  <div class="row">
    <div class="col-md-12">
      <h4 class="">Informar Plantonistas do Dia</h4>
    </div>
  </div>
  <div class="row">
    <div class ="col-md-8">
      <br/>
      <label for="curso">Selecione os voluntários do atendimento presentes hoje:</label>
      <select class="custom-select d-block w-100" name="voluntarios" id="voluntarios" required="">
        <option value="">Escolha...</option>
        <?php 
          while ($linha_voluntario = $res_voluntario -> fetch_assoc()){
            ?>
              <option value="<?=$linha_voluntario['codigo']?>"><?=utf8_encode($linha_voluntario['nome'])?>
              </option>
            <?php 
          }  
      ?>
      </select>
    </div>
    <button class="btn btn-primary btn-md" style="margin:25px" type="submit">Adicionar</button>
    
  </div> 

  <section class="plantonista">
  <br/><br/>
          <h6 class="center">Presentes hoje:</h3>

          <div class="bd-example" style="background-color:#ffffff">
            <table class="table centered bordered table-striped">
                <thead>
                    <tr>
                        <th>Código</th>
                        <th>Nome</th>
                        <th>Remover</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </section>
        </section>
  <script src="popper.min.js" crossorigin="anonymous"></script>
  <script src="bootstrap-4.0.0-beta.3/dist/js/bootstrap.min.js" crossorigin="anonymous"></script>
  <script src="plantonista.js"></script>  
</form>