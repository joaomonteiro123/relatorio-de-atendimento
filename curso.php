<?php

include "conectasql.php";

$cursos_filtro = "SELECT DISTINCT e.* FROM `evento` e,turma t where DATE(t.DATA_INICIAL) >= CURDATE() and e.id = t.ID_EVENTO ORDER BY NOME";
$res_curso = $conexao ->query($cursos_filtro);

?>

<script src="jquery-ui.js"></script>

<script>
$(document).ready(function () {
  atualizaVendas(0,"recuperaCursos.php");
  $( "#comprador" ).autocomplete({
    source: "listanomes.php",
    minLength: 6,
  });

  $('#curso_selecionado').change(function(){
    $.ajax({
      url: 'buscaturma.php',
      type: 'post',
      data: "curso_selecionado="+$("#curso_selecionado").val(),
      dataType: 'json',
      success: function(turmas){
        $("#turma_selecionada").empty();
        $("#turma_selecionada").append(
          $('<option></option>').val('').html('Escolha...')
        );
        var i;
        for (i = 0; i < turmas.length; ++i) {
          $("#turma_selecionada").append(
            $('<option></option>').val(turmas[i]['id']).html(turmas[i]['label']+" - "+turmas[i]['data'])
          );
        }
      }
    });
  });
  $('form').on('submit', function (e) {
    var tel = document.getElementById("telefone").value;
    var cpf = document.getElementById("cpf").value;
    var email = document.getElementById("email").value;
    if(codigo == -1 && tel.trim() == '' && cpf.trim() == '' && email.trim() == ''){
        e.preventDefault(); //prevent to reload the page
        alert('Como o aluno não está no IIPCNET, informe pelo menos 1 desses campos: Telefone, Email ou CPF');
    }else{
    e.preventDefault(); //prevent to reload the page
    $.ajax({
        type: 'POST', //hide url
        url: 'enviaVenda.php', //your form validation url
        data: $('form').serialize()+"&cod_iipcnet="+$("#atendidoID").val()+"&tipo_venda="+tipo_venda+"&fl_sem_iipcnet="+$("#fl_sem_iipcnet").val(),
        success: function () {
            alert('Enviado com sucesso!'); //display an alert whether the form is submitted okay
            //location.reload();
            var sel = document.getElementById("curso_selecionado");
            var nome_curso = sel.options[sel.selectedIndex].text;
            insereVendas(nome_curso,$("#atendido").val(),$("#valor").val());
            $("#frm_curso")[0].reset();
        }
    });
    }
  });
});
</script>

<form id="frm_curso">
  <div class="row">
    <div class="col-md-12">
      <h4 class="">Venda de Curso</h4>
    </div>
  </div>

  <?php
    include "comprador.php";
  ?>

  <div class ="row mb-3">
    <div class ="col-md-6">
      <label for="curso">Nome do curso</label>
      <select class="custom-select d-block w-100" name="curso_selecionado" id="curso_selecionado" required="">
        <option value="">Escolha...</option>
        <?php 
          while ($linha_curso = $res_curso -> fetch_assoc()){
            ?>
              <option value="<?=$linha_curso['id']?>"><?=utf8_encode($linha_curso['NOME'])?>
              </option>
            <?php 
          }  
      ?>
      </select>
    </div>
    <div class="col-md-6">
      <label for="turma">Turma</label>
      <select class="custom-select d-block w-100" name="turma" id="turma_selecionada" required="">
        <option value="">Escolha...</option>
      </select>
    </div>
  </div>

  <?php
    include "origem_aluno.php";
  ?>
  
  <div class="row">
    <div class="col-md-12">
      <h4 class="">Pagamento</h4>
    </div>
  </div>
  
  <?php
    include "pagamento.php";
  ?>

  <div class="row">
    <button class="btn btn-primary btn-lg btn-block" type="submit">Enviar</button>
  </div>
  <?php
    include "vendas_lancadas.php";
  ?>

  <script src="popper.min.js" crossorigin="anonymous"></script>
  <script src="vendas_lancadas.js"></script>  
  <script src="bootstrap-4.0.0-beta.3/dist/js/bootstrap.min.js" crossorigin="anonymous"></script>
</form>