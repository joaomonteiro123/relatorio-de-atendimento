<script>
    var codigo = 0;
    $(document).ready(function () {
        $( "#atendido" ).autocomplete({
            source: "listanomes.php",
            minLength: 3,
            select: function (event, ui) {
                $("#atendidoID").val(ui.item.id); 
                $("#fl_sem_iipcnet").val(ui.item.fl_sem_iipcnet);
            },
            change: function(event, ui) {
                if (ui.item == null) {                   
                    $("#fl_sem_iipcnet").val(1);
                    $("#div_aluno_sem_cadastro").show();
                }else{
                    $("#div_aluno_sem_cadastro").hide();
                    // está cadaastrado
                }
            }  
        });
    });
</script>


<div class="row">
    <div class="col-md-12 mb-3">
        <label for="atendido">Nome completo do comprador</label>
        <input type="text" class="form-control" id="atendido" name="atendido" placeholder="" value="" required="">
        <input type="hidden" class="form-control" id="atendidoID" placeholder="" value="0" required="">
        <input type="hidden" class="form-control" id="fl_sem_iipcnet" required="">
    </div>
</div>

<div id="div_aluno_sem_cadastro" class="card mb-4" style="display: none;">
    <div class="row mb-2">
        <div class="col-md-12" style="text-align: center;">
            <label style="color: red; margin-top: 15px; font-size: 15px;">Comprador sem cadastro no IIPCNet</label>
        </div>
    </div>
    <div class="row mb-2">
        <div class="col-md-5 offset-1">
            <label for="telefone">Telefone do comprador</label>
            <input type="text" style="background-color: white;" class="form-control input-lg" id="telefone" name="telefone">
        </div>
        <div class="col-md-5">
            <label for="cpf">CPF do comprador</label>
            <input type="number" style="background-color: white;" class="form-control input-lg" id="cpf" name="cpf">
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-md-10 offset-1">
            <label for="email">E-mail do comprador</label>
            <input type="text" style="background-color: white;" class="form-control input-lg" id="email" name="email">
        </div>
    </div>
</div>