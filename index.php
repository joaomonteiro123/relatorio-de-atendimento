<HTML>
<HEAD>
        <TITLE>IIPC BH - Relatório de Atendimento</TITLE>
        <link rel="stylesheet" href="bootstrap-4.0.0-beta.3/dist/css/bootstrap.min.css">
        <script src="jquery-3.3.1.min.js" crossorigin="anonymous"></script>
        <script src="jquery-ui.js"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
                <script src="jquery-maskmoney-master/dist/jquery.maskMoney.min.js" type="text/javascript"></script>
                <script src="jquery-maskmoney-master/dist/jquery.maskMoney.js" type="text/javascript"></script>

        <script>
        var tipo_atendimento;
        var tipo_venda;
                $(document).ready(function () {       
                        
                        $("#form").load("plantonista.php?scroll=0");

                        $('#livro').click(function () {
                                if ($(this).is(':checked')) {
                                        $("#form").load("livro.php");
                                        tipo_venda = 1;
                                }
                        });
                        $('#curso').click(function () {
                                if ($(this).is(':checked')) {
                                        $("#form").load("curso.php");
                                        tipo_venda = 2;
                                }
                        });
                        $('#at_telefone').click(function () {
                                if ($(this).is(':checked')) {
                                        tipo_atendimento = 1;
                                        $("#form").load("telefone.php");
                                }
                        });
                        $('#at_chat').click(function () {
                                if ($(this).is(':checked')) {
                                        tipo_atendimento = 2;
                                        $("#form").load("chat.php");
                                }
                        });
                        $('#at_presencial').click(function () {
                                if ($(this).is(':checked')) {
                                        tipo_atendimento = 3;
                                        $("#form").load("presencial.php");
                                }
                        });
                        $('#tenepes').click(function () {
                                if ($(this).is(':checked')) {
                                        $("#form").load("tenepes.php");
                                }
                        });
                        $('#encomenda').click(function () {
                                if ($(this).is(':checked')) {
                                        $("#form").load("encomenda.php");
                                }
                        });
                        $('#plantonista').click(function () {
                                if ($(this).is(':checked')) {
                                        $("#form").load("plantonista.php?scroll=1");
                                        
                                }
                        });
                        
                });
        </script>
</HEAD>

<BODY class="bg-light">
        <div class="container">
                <div class="py-5 text-center">
                        <img class="d-block mx-auto mb-4" src="Images/IIPC.png" alt="" width="200">
                        <h2>Relatório de Atendimento</h2>
                        <p class="lead">Bem vindo! Lembre-se de informar seu nome e dos colegas do atendimento que estiverem presentes hoje.
                        </p>
                </div>
                <div class="row">
                        <div class="col-md-4" style="border-right: 1px solid #333; height: 220.8px;">                               
                                <h4 class="mb-3">Ocorrência</h4> 
                                <div class="d-block my-3">
                                        <div class="custom-control custom-radio">
                                                <input id="livro" name="ocorrencia" type="radio" class="custom-control-input" required="">
                                                <label class="custom-control-label" for="livro">Venda de livro</label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                                <input id="curso" name="ocorrencia" type="radio" class="custom-control-input" required="">
                                                <label class="custom-control-label" for="curso">Venda de curso</label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                                <input id="at_telefone" name="ocorrencia" type="radio" class="custom-control-input" required="">
                                                <label class="custom-control-label" for="at_telefone">Atendimento telefônico</label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                                <input id="at_chat" name="ocorrencia" type="radio" class="custom-control-input" required="">
                                                <label class="custom-control-label" for="at_chat">Atendimento por chat</label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                                <input id="at_presencial" name="ocorrencia" type="radio" class="custom-control-input" required="">
                                                <label class="custom-control-label" for="at_presencial">Atendimento presencial</label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                                <input id="tenepes" name="ocorrencia" type="radio" class="custom-control-input" required="">
                                                <label class="custom-control-label" for="tenepes">Pedido de Tenepes</label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                                <input id="encomenda" name="ocorrencia" type="radio" class="custom-control-input" required="">
                                                <label class="custom-control-label" for="encomenda">Recebimento de correspondência</label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                                <input id="plantonista" name="ocorrencia" type="radio" class="custom-control-input" required="">
                                                <label class="custom-control-label" for="plantonista">Informar Plantonistas</label>
                                        </div>
                                </div>
                        </div>
                        <div class="col-md-7 offset-1" id="form"></div>
                </div>
        </div>
        <script src="popper.min.js" crossorigin="anonymous"></script>
        <script src="bootstrap-4.0.0-beta.3/dist/js/bootstrap.min.js" crossorigin="anonymous"></script>
</BODY>

</HTML>